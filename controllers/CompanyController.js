const Test = require('../models/Test');
const {Company} = require("../models/Company");
const jwt = require('jsonwebtoken');  // used to si

const bcrypt = require('bcrypt');

const register = async (request, h) =>{

    const { name,username,confPassword , email, password } = request.payload;
    // //
    // return h.response({
    //     data : name
    // },300);
    // return;
    if(password !== confPassword){

        return h.response({
            message : 'passwords are not the same',
            data : null,
            status : "danger"
        }).code(400);
    }

    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);
    try {
        const company = await Company.create({
            company_name: name,
            company_username: username,
            company_email: email,
            company_password: hashPassword
        });

        return h.response({
            message : 'success Register',
            data : null,
            status : "success"
        });

    } catch (error) {
        return h.response({
            message : error.errors[0].message,
            data : null,
            status : "danger",
            statusCode : 200

        });
    }

}

const login = async (request, h) =>{
    const { username, password } = request.payload;

    try {
        const company = await Company.findOne({ where: { company_username: username } });


        const match = await bcrypt.compare(password, company.company_password);

        // return match;
        if(!match){
            return h.response({
                message : 'Wrong Password',
                data : null,
                status : "danger"
            }).code(400)
        }
        const userId = company.company_id;
        const name = company.company_name;
        const email = company.company_email;
        const accessToken = jwt.sign({userId, name, email}, process.env.ACCESS_TOKEN_SECRET,{
            expiresIn: '100s'
        });

        await Company.update({company_refresh_token: accessToken},{
            where:{
                company_id: userId
            }
        });

        return h.response({
            message : 'success Login',
            data : {
                token : accessToken
            },
            status : "success",
            statusCode : 200
        });

        // res.json({ data : accessToken , msg:"Berhasil Login",status : "success"});
    } catch (error) {

        // res.status(404).json({msg:"Email tidak ditemukan",status : "error"});
    }

}

const getALl = async (request, h)=>{

    try {
        const company = await Company.findAll({
            attributes: ['company_name']
        });

        return h.response({
            data : company
        });

    } catch (error) {
        console.log(error);
    }

}

const cek = async (request, h)=>{
    let cookie = request.state
    // console.log(request.state.data);
    return h.response({
        message : 'success Login',
        data : {
            token : cookie
        },
        status : "success"
    });
}

module.exports = { register , login ,cek }

