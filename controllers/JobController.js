const Test = require('../models/Test');
const {Company} = require("../models/Company");
const jwt = require('jsonwebtoken');  // used to si

const bcrypt = require('bcrypt');
const {Job} = require("../models/Job");

const getAllJob = async (request, h)=>{

    try {
        // const jobs = await Job.findAll({
        //     attributes: ['job_title']
        // });
        const jobs = await Job.findAll();

        return h.response({
            data : jobs
        });

    } catch (error) {
        console.log(error);
    }

}

const getDetailJob = async (request, h)=>{

    try {
        const param = request.params;

        const job = await Job.findOne({ where: { job_id: param.job_id } });

        return h.response({
            data : job
        });

    } catch (error) {
        console.log(error);
    }

}


module.exports = { getAllJob,getDetailJob }

