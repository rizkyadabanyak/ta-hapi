// const index = require('../controllers/AdminController.js');
const company = require('../controllers/CompanyController.js');
// const job = require('../controllers/JobController.js');

const rotues = [
    {
        method: 'POST',
        path: '/company/register',
        config: {
            auth: false,
            payload: {
                parse: true,
                allow: 'multipart/form-data',
                multipart: { output: 'stream' },
            }
        },
        handler: company.register
    },
    // {
    //     method: 'POST',
    //     path: '/company/login',
    //     config: {
    //         auth: false,
    //         payload: {
    //             parse: true,
    //             allow: 'multipart/form-data',
    //             multipart: { output: 'stream' },
    //         },
    //         state: {
    //             parse: true,
    //             failAction: 'error'
    //         }
    //     },
    //     handler: company.login
    // },
    // {
    //     method: 'POST',
    //     path: '/company/getAllData',
    //     config: {
    //         auth: false,
    //         state: {
    //             parse: true,
    //             failAction: 'error'
    //         }
    //     },
    //     handler: company.login
    // },
    // {
    //     method: 'GET',
    //     path: '/jobs/detail/{job_id}',
    //     config: {
    //         auth: 'jwt',
    //         state: {
    //             parse: true,
    //             failAction: 'error'
    //         }
    //     },
    //     handler: job.getDetailJob
    // },
    // {
    //     method: 'GET',
    //     path: '/authCoba',
    //     config: { auth: 'jwt' },
    //     handler: (request, h) => {
    //         return 'Hello Worlddqw!';
    //     }
    // },
    // {
    //     method: 'GET',
    //     path: '/test',
    //     config: { auth: 'jwt' },
    //     handler: (request, h) => {
    //         return 'ywy';
    //     }
    // },
];

module.exports = { rotues }
